package no.dips.bifrost.codegenerator;

import org.openehr.schemas.v1.OPERATIONALTEMPLATE;
import org.openehr.schemas.v1.TemplateDocument;

import java.io.InputStream;

public class OptLoader {

    public static OPERATIONALTEMPLATE fromInputStream(InputStream is){
        try {
            TemplateDocument d = TemplateDocument.Factory.parse(is);
            return d.getTemplate();
        }catch (Exception e){
            throw new RuntimeException(e);

        }
    }
}
