package no.dips.bifrost.codegenerator;

import com.nedap.archie.rm.composition.Composition;
import org.apache.commons.io.IOUtils;
import org.ehrbase.serialisation.xmlencoding.CanonicalXML;

import java.io.IOException;
import java.io.InputStream;

public class CompositionLoader {

    public static Composition load(InputStream is) throws IOException {
        String content = IOUtils.toString(is, "UTF-8");
        CanonicalXML xml = new CanonicalXML();
        return xml.unmarshal(content, Composition.class);
    }
}
