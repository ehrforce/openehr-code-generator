package no.dips.bifrost.codegenerator;

import org.openehr.schemas.v1.ARCHETYPETERM;
import org.openehr.schemas.v1.CARCHETYPEROOT;
import org.openehr.schemas.v1.StringDictionaryItem;

import java.util.Optional;

public class TermFinder {

    private final CARCHETYPEROOT root;

    public CARCHETYPEROOT getRoot() {
        return root;
    }

    public TermFinder(CARCHETYPEROOT root) {
        this.root = root;
    }

    public Optional<String> getText(String nodeId) {
        return getTermForNode(nodeId, "text");
    }
    public Optional<String> getDescription(String nodeId){
        return getTermForNode(nodeId, "description");
    }
    private Optional<String> getTermForNode(String nodeId, String key){
        for (ARCHETYPETERM t : root.getTermDefinitionsArray()) {
            if (nodeId.contentEquals(t.getCode())) {
                for (StringDictionaryItem item : t.getItemsArray()) {
                    if (item.getId().contentEquals(key)) {
                        return Optional.of(item.getStringValue());
                    }
                }
            }
        }
        return Optional.empty();

    }
}
