package no.dips.bifrost.codegenerator;


import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@NoArgsConstructor

public class Field {
    private String archetypeId;
    private String nodeId;
    private String rmType;
    private String valueType;
    private String name;
private String description;
    private String path;
    private boolean isSingle = true;


    public int level(){
        if(path == null){
            return -1;
        }else{
            return path.split("/").length;
        }
    }

}
