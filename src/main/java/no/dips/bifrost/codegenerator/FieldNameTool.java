package no.dips.bifrost.codegenerator;

import com.google.common.base.CaseFormat;
import com.google.common.base.CharMatcher;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

/**
 * Tool to generate reasonable Java field and class names based on strings given as OPT names
 */
public class FieldNameTool {



    public static String toFieldName(String s){
        return CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.LOWER_CAMEL, normalize(s));
    }
    public static String toClassName(String s){

        return CaseFormat.LOWER_UNDERSCORE.to(CaseFormat.UPPER_CAMEL,normalize(s));
    }

    public static String normalize(String s){
        if(s == null){
            return RandomStringUtils.randomAlphabetic(10).toLowerCase();
        }


        s = s.toLowerCase();
        s = CharMatcher.whitespace().replaceFrom(s, "_");
        s = CharMatcher.anyOf("()").removeFrom(s);
        s = CharMatcher.anyOf(".").replaceFrom(s, "_");
        s = CharMatcher.is('æ').replaceFrom(s, "ae");
        s = CharMatcher.is('ø').replaceFrom(s, "oe");
        s = CharMatcher.is('å').replaceFrom(s, "aa");
        s = StringUtils.strip(StringUtils.stripAccents(s).replace("ß", "ss").replaceAll("[^A-Za-z0-9]", "_"), "_");
        if (StringUtils.isBlank(s) || s.equals("_")) {
            return RandomStringUtils.randomAlphabetic(10).toLowerCase();
        }

        return s;
    }

}
