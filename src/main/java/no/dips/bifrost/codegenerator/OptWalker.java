package no.dips.bifrost.codegenerator;

import org.apache.commons.lang3.StringUtils;
import org.openehr.schemas.v1.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


public class OptWalker {
    private static final String [] EXPORTABLE_FIELDS = new String[] {"ELEMENT"}; //, "CLUSTER"
    private Logger log = LoggerFactory.getLogger(OptWalker.class);
    private final OPERATIONALTEMPLATE opt;
    private List<String> paths = new ArrayList<>();

    public List<Field> getFields() {
        return fields;
    }

    private List<Field> fields = new ArrayList<>();

    public OptWalker(OPERATIONALTEMPLATE opt) {

        this.opt = opt;
    }

    public List<String> getPaths() {
        return this.paths;
    }

    public OptWalker walk() {
        paths = new ArrayList<>();
        fields = new ArrayList<>();
        CARCHETYPEROOT root = opt.getDefinition();
        TermFinder termFinder = new TermFinder(root);
        for (CATTRIBUTE a : root.getAttributesArray()) {
            String path = "/" + a.getRmAttributeName();
            for (COBJECT c : a.getChildrenArray()) {
                walk(c, path, termFinder);
            }
        }

        return this;
    }

    private void walk(COBJECT c, String path, TermFinder termFinder) {
        if (c instanceof CARCHETYPEROOT) {
            CARCHETYPEROOT r = (CARCHETYPEROOT) c;
            path = path + "[" + r.getArchetypeId().getValue() + "]";
            termFinder = new TermFinder(r);
        } else {
            if (c.getNodeId() != null && c.getNodeId().length() > 0) {
                path = path + "[" + c.getNodeId() + "]";
            }
        }
        Optional<String> name = termFinder.getText(c.getNodeId());
        Optional<String> description = termFinder.getDescription(c.getNodeId());
        Field field = new Field();
        field.setArchetypeId(termFinder.getRoot().getArchetypeId().getValue());
        field.setName(name.orElse("NotSet"));
        field.setDescription(description.orElse("NotSet"));
        field.setPath(path);
        field.setRmType(c.getRmTypeName());
        field.setNodeId(c.getNodeId());

        if("ELEMENT".contentEquals(c.getRmTypeName())){
            CCOMPLEXOBJECT cc = (CCOMPLEXOBJECT)c;
            field.setSingle(isSingle(cc));
            boolean isSingle = isSingle(cc);
            for(CATTRIBUTE a: cc.getAttributesArray()){
                if("value".contentEquals(a.getRmAttributeName())) {
                    if(a.getChildrenArray().length > 1){
                        log.warn("Choiche element - not sure how to handle that");
                    }
                    if(a.getChildrenArray().length > 0){
                        COBJECT value = a.getChildrenArray(0);
                        field.setValueType(value.getRmTypeName());
                    }
                }
            }
        }

        if (StringUtils.containsAny(c.getRmTypeName(), EXPORTABLE_FIELDS)) {
              fields.add(field);
        }
        if (c instanceof CCOMPLEXOBJECT) {
            CCOMPLEXOBJECT cc = (CCOMPLEXOBJECT) c;
            IntervalOfInteger occurences =  c.getOccurrences();
            for (CATTRIBUTE a : cc.getAttributesArray()) {
                if (a instanceof CMULTIPLEATTRIBUTE) {
                    CMULTIPLEATTRIBUTE cmultipleattribute = (CMULTIPLEATTRIBUTE) a;
                    CARDINALITY cardinality = cmultipleattribute.getCardinality();
                    IntervalOfInteger interval = cardinality.getInterval();

                }
                String currentPath = path + "/" + a.getRmAttributeName();
                for (COBJECT child : a.getChildrenArray()) {
                    walk(child, currentPath, termFinder);
                }
            }
        }

    }

    private boolean isSingle(CCOMPLEXOBJECT cc) {
        if(cc.getOccurrences() != null){
            IntervalOfInteger intervalOfInteger =  cc.getOccurrences();
            if(intervalOfInteger.getUpperUnbounded()){
                return false;
            }
        }
        return true;
    }

    private boolean isLocatable(COBJECT c) {
        if (c instanceof CARCHETYPEROOT) {
            return true;
        }
        if (c.getNodeId() != null && c.getNodeId().length() > 0) {
            return true;
        }
        return false;
    }
}
