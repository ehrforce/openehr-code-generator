package no.dips.bifrost.codegenerator;

import com.nedap.archie.query.RMPathQuery;
import com.nedap.archie.rm.composition.Composition;
import com.nedap.archie.rminfo.ArchieRMInfoLookup;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Field;
import java.util.List;

public class TemplateDocumentLoader {
    private Logger log = LoggerFactory.getLogger(TemplateDocumentLoader.class);

    public <T> T load(Composition c, Class<T> clazz) {
        try {
            T obj = clazz.getDeclaredConstructor().newInstance();

            for (Field f : clazz.getDeclaredFields()) {
                if (f.isAnnotationPresent(AqlPath.class)) {
                    AqlPath fa = f.getAnnotation(AqlPath.class);
                    String path = fa.path();
                    boolean isSingle = fa.isSingle();
                    if (isSingle) {
                        Object result = c.itemAtPath(path);
                        f.setAccessible(true);
                        f.set(obj, result);

                    } else {
                        List<Object> results = c.itemsAtPath(path);
                        f.setAccessible(true);
                        f.set(obj,results);
                    }
                }
            }
            return obj;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }


}
