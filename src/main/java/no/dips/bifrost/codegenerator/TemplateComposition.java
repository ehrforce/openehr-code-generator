package no.dips.bifrost.codegenerator;

public interface TemplateComposition {

    String getTemplateId();
}
