package no.dips.bifrost.codegenerator;


import com.nedap.archie.rminfo.ArchieRMInfoLookup;
import com.squareup.javapoet.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.openehr.schemas.v1.OPERATIONALTEMPLATE;
import org.openehr.schemas.v1.TemplateDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.lang.model.element.Modifier;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Type;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CodeGenerator {
    private Logger log = LoggerFactory.getLogger(CodeGenerator.class);
    private final OPERATIONALTEMPLATE opt;

    public CodeGenerator(OPERATIONALTEMPLATE opt) {

        this.opt = opt;
    }

    public void generateClasses(String packageName, String toDirectory) throws IOException {
        OptWalker walker = new OptWalker(opt);
        String className = FieldNameTool.toClassName(opt.getTemplateId().getValue());
        log.info("Using class name = " + className);
        List<Field> fields = walker.walk().getFields();

        List<FieldSpec> fieldSpecs = new ArrayList<>();
        Map<String, Integer> names = new HashMap<>();

        for(Field f: fields){
            String rmType = f.getValueType() == null? f.getRmType() : f.getValueType();
            Class fieldClass = ArchieRMInfoLookup.getInstance().getClassToBeCreated(rmType);
            String fieldName = FieldNameTool.toFieldName(f.getName());
            String path = f.getPath() + "/value";
            if(names.containsKey(fieldName)){
                int count = names.get(fieldName);
                count = count +1;
                names.put(fieldName, count);
                fieldName = fieldName + "_" + count;
            }else{
                names.put(fieldName, 0);
            }
            if(f.isSingle()) {
                FieldSpec spec = FieldSpec.builder(fieldClass, fieldName)
                        .addModifiers(Modifier.PRIVATE)
                        .addJavadoc("Name: $S \nDescription: $S\nIsSingle $S", f.getName(), f.getDescription(), f.isSingle())
                        .addAnnotation(AnnotationSpec.builder(AqlPath.class)
                                .addMember("path", "$S", path)
                                .addMember("isSingle", "$L", true)
                                .build())
                        .build();
                fieldSpecs.add(spec);
            }else{
                FieldSpec spec = FieldSpec
                        .builder(ParameterizedTypeName.get(List.class, fieldClass),fieldName)
                        .addModifiers(Modifier.PRIVATE)
                        .addAnnotation(AnnotationSpec
                                .builder(AqlPath.class)
                                .addMember("path",  "$S", path )
                                .addMember("isSingle", "$L", false)
                                .build()
                        )
                        .addJavadoc("Name: $S \nDescription: $S\nIsSingle $S", f.getName(), f.getDescription(), f.isSingle())
                        .build();
                fieldSpecs.add(spec);

            }
        }
        MethodSpec templateId = MethodSpec.methodBuilder("getTemplateId")
                .addModifiers(Modifier.PUBLIC).addAnnotation(Override.class)
                .returns(String.class)
                .addStatement("return $S", opt.getTemplateId().getValue())
                .build();

        TypeSpec templateClass = TypeSpec.classBuilder(className)
                .addModifiers(Modifier.PUBLIC)
                .addSuperinterface(TemplateComposition.class)
                .addAnnotation(Data.class)
                .addAnnotation(NoArgsConstructor.class)
                .addAnnotation(AllArgsConstructor.class)
                .addAnnotation(AnnotationSpec.builder(Template.class)
                        .addMember("templateId", "$S", opt.getTemplateId().getValue())
                        .build())
                .addMethod(templateId)
                .addFields(fieldSpecs)
                .build();

        JavaFile javaFile = JavaFile.builder(packageName, templateClass)
                .build();
        javaFile.writeTo(Paths.get(toDirectory));


    }

    public static CodeGenerator fromFile(String path) {
        try {
            TemplateDocument d = TemplateDocument.Factory.parse(new File(path));
            return new CodeGenerator(d.getTemplate());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static CodeGenerator fromStream(InputStream is) {
        try {
            TemplateDocument d = TemplateDocument.Factory.parse(is);
            return new CodeGenerator(d.getTemplate());
        } catch (Exception e) {
            throw new RuntimeException(e);

        }
    }
}
