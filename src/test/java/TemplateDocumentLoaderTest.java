import com.nedap.archie.rm.composition.Composition;
import com.nedap.archie.rm.datavalues.DvCodedText;
import no.dips.bifrost.codegenerator.CompositionLoader;
import no.dips.bifrost.codegenerator.TemplateDocumentLoader;
import no.dips.testing.KreftregisteretKolorektalskjemaKjemoterapiv2;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class TemplateDocumentLoaderTest {

    @Test
    public void testLoadClassInstance() throws IOException {
        TemplateDocumentLoader loader = new TemplateDocumentLoader();
        InputStream is = TemplateDocumentLoaderTest.class.getResourceAsStream("/compositions/kjemoterapiv1_01.xml");
        Composition c = CompositionLoader.load(is);
        KreftregisteretKolorektalskjemaKjemoterapiv2 k = loader.load(c, KreftregisteretKolorektalskjemaKjemoterapiv2.class);

        Assertions.assertNotNull(k);
        Assertions.assertEquals("Kreftregisteret Kolorektalskjema kjemoterapiV2",k.getTemplateId());

        List<DvCodedText> metastaser=  k.getLokalisasjonAvMetastaser();
        Assertions.assertNotNull(metastaser);
        Assertions.assertEquals(3, metastaser.size());
        metastaser.forEach( m -> {
            System.out.println(m.getValue());
        });

        DvCodedText behFor = k.getBehandlingFor();
        Assertions.assertEquals("at0341", behFor.getDefiningCode().getCodeString());

    }
}
