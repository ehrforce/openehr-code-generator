import com.ibm.icu.impl.Assert;
import com.nedap.archie.rules.Assertion;
import no.dips.bifrost.codegenerator.FieldNameTool;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FieldNameToolTest {


    @Test
    public void testNormalize() {
        assertNormalize("hei bjørn", "hei_bjoern");
        assertNormalize("hei.bjoern", "hei_bjoern");
        assertNormalize("æøå", "aeoeaa");
        assertNormalize("£$abc", "abc");
    }

    @Test
    public void testFieldName() {
        assertFieldName("Hei Bjørn", "heiBjoern");
    }
    @Test
    public void testClassName(){
        assertClassName("hei bjørn", "HeiBjoern");
    }

    private void assertNormalize(String input, String shouldBe) {
        Assertions.assertEquals(shouldBe, FieldNameTool.normalize(input));
    }

    private void assertFieldName(String input, String shouldBe) {
        Assertions.assertEquals(shouldBe, FieldNameTool.toFieldName(input));
    }
    private void assertClassName(String input, String shouldBe){
        Assertions.assertEquals(shouldBe, FieldNameTool.toClassName(input));
    }

}
