import no.dips.bifrost.codegenerator.Field;
import no.dips.bifrost.codegenerator.OptLoader;
import no.dips.bifrost.codegenerator.OptWalker;
import org.junit.jupiter.api.Test;
import org.openehr.schemas.v1.OPERATIONALTEMPLATE;

import java.io.InputStream;
import java.util.List;

public class OptWalkerTest {

    @Test
    public void testLoadKjemo(){
        InputStream is = OptWalkerTest.class.getResourceAsStream("/opts/Kolorektal kreftmelding kirurgi2.opt");
        OPERATIONALTEMPLATE opt = OptLoader.fromInputStream(is);
        OptWalker walker = new OptWalker(opt);
        List<Field> fields =  walker.walk().getFields();
        fields.stream().filter(f -> !f.isSingle()).forEach(f -> System.out.println(f));
    }
}
