import no.dips.bifrost.codegenerator.CodeGenerator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.InputStream;

public class CodeGeneratorTest {

    @Test
    public void testLoadKjemo() throws IOException {
        InputStream is = CodeGeneratorTest.class.getResourceAsStream("/opts/Kreftregisteret Kolorektalskjema kjemoterapiV2.opt");
        CodeGenerator generator = CodeGenerator.fromStream(is);
        Assertions.assertNotNull(generator);
        generator.generateClasses("no.dips.testing","target/generated");
    }
}
