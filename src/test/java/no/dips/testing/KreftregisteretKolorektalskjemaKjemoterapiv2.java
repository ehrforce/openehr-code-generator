package no.dips.testing;

import com.nedap.archie.rm.datavalues.DvBoolean;
import com.nedap.archie.rm.datavalues.DvCodedText;
import com.nedap.archie.rm.datavalues.DvText;
import com.nedap.archie.rm.datavalues.quantity.DvQuantity;
import com.nedap.archie.rm.datavalues.quantity.datetime.DvDate;
import com.nedap.archie.rm.datavalues.quantity.datetime.DvDateTime;
import java.lang.Override;
import java.lang.String;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import no.dips.bifrost.codegenerator.AqlPath;
import no.dips.bifrost.codegenerator.Template;
import no.dips.bifrost.codegenerator.TemplateComposition;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Template(
        templateId = "Kreftregisteret Kolorektalskjema kjemoterapiV2"
)
public class KreftregisteretKolorektalskjemaKjemoterapiv2 implements TemplateComposition {
  /**
   * Name: "Klar til sending"
   * Description: "*"
   * IsSingle "true"
   */
  @AqlPath(
          path = "/context/other_context[at0001]/items[at0003]/value",
          isSingle = true
  )
  private DvBoolean klarTilSending;

  /**
   * Name: "Meldingstype"
   * Description: "*"
   * IsSingle "true"
   */
  @AqlPath(
          path = "/context/other_context[at0001]/items[at0002]/value",
          isSingle = true
  )
  private DvText meldingstype;

  /**
   * Name: "Behandling for"
   * Description: "*"
   * IsSingle "true"
   */
  @AqlPath(
          path = "/content[openEHR-EHR-OBSERVATION.kreftmelding_kolorektalkreft_kjemoterapi_dips.v1]/data[at0001]/events[at0002]/data[at0003]/items[at0337]/items[at0339]/value",
          isSingle = true
  )
  private DvCodedText behandlingFor;

  /**
   * Name: "Lokalisasjon av metastaser"
   * Description: "*"
   * IsSingle "false"
   */
  @AqlPath(
          path = "/content[openEHR-EHR-OBSERVATION.kreftmelding_kolorektalkreft_kjemoterapi_dips.v1]/data[at0001]/events[at0002]/data[at0003]/items[at0337]/items[at0402]/value",
          isSingle = false
  )
  private List<DvCodedText> lokalisasjonAvMetastaser;

  /**
   * Name: "Spesifiser lokalisasjon"
   * Description: "*"
   * IsSingle "true"
   */
  @AqlPath(
          path = "/content[openEHR-EHR-OBSERVATION.kreftmelding_kolorektalkreft_kjemoterapi_dips.v1]/data[at0001]/events[at0002]/data[at0003]/items[at0337]/items[at0352]/value",
          isSingle = true
  )
  private DvText spesifiserLokalisasjon;

  /**
   * Name: "Lokalisajon"
   * Description: "Lokalisasjon av tumor "
   * IsSingle "true"
   */
  @AqlPath(
          path = "/content[openEHR-EHR-OBSERVATION.kreftmelding_kolorektalkreft_kjemoterapi_dips.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.kreftmelding_kolorektal_lokalisasjon_av_primaertumor_dips.v1]/items[at0001]/value",
          isSingle = true
  )
  private DvCodedText lokalisajon;

  /**
   * Name: "Spesifiser lokalisajon Colon"
   * Description: "Angir spesifikk lokalisasjon for Colon "
   * IsSingle "true"
   */
  @AqlPath(
          path = "/content[openEHR-EHR-OBSERVATION.kreftmelding_kolorektalkreft_kjemoterapi_dips.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.kreftmelding_kolorektal_lokalisasjon_av_primaertumor_dips.v1]/items[at0004]/value",
          isSingle = true
  )
  private DvCodedText spesifiserLokalisajonColon;

  /**
   * Name: "Angi avstand fra analåpning til nedre kant av tumor (målt på stivt skop)"
   * Description: "Angir avstand fra analåpning til nedre kant av tumor (målt på stivt skop)"
   * IsSingle "true"
   */
  @AqlPath(
          path = "/content[openEHR-EHR-OBSERVATION.kreftmelding_kolorektalkreft_kjemoterapi_dips.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.kreftmelding_kolorektal_lokalisasjon_av_primaertumor_dips.v1]/items[at0026]/value",
          isSingle = true
  )
  private DvQuantity angiAvstandFraAnalaapningTilNedreKantAvTumorMaaltPaaStivtSkop;

  /**
   * Name: "Ukjent"
   * Description: "Avstand ukjent. "
   * IsSingle "true"
   */
  @AqlPath(
          path = "/content[openEHR-EHR-OBSERVATION.kreftmelding_kolorektalkreft_kjemoterapi_dips.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.kreftmelding_kolorektal_lokalisasjon_av_primaertumor_dips.v1]/items[at0033]/value",
          isSingle = true
  )
  private DvBoolean ukjent;

  /**
   * Name: "Avstand fra øvre kant av M.puborektalis til nedre kant av tumor (målt ved MR)"
   * Description: "Angir avstand fra øvre kant av M.puborektalis til nedre kant av tumor (målt ved MR)"
   * IsSingle "true"
   */
  @AqlPath(
          path = "/content[openEHR-EHR-OBSERVATION.kreftmelding_kolorektalkreft_kjemoterapi_dips.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.kreftmelding_kolorektal_lokalisasjon_av_primaertumor_dips.v1]/items[at0034]/value",
          isSingle = true
  )
  private DvQuantity avstandFraOevreKantAvMPuborektalisTilNedreKantAvTumorMaaltVedMr;

  /**
   * Name: "Ukjent"
   * Description: "Avstand ukjent. "
   * IsSingle "true"
   */
  @AqlPath(
          path = "/content[openEHR-EHR-OBSERVATION.kreftmelding_kolorektalkreft_kjemoterapi_dips.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.kreftmelding_kolorektal_lokalisasjon_av_primaertumor_dips.v1]/items[at0032]/value",
          isSingle = true
  )
  private DvBoolean ukjent_1;

  /**
   * Name: "Klinisk sikker kreft"
   * Description: "Angir informasjon om kreft er klinisk sikker. "
   * IsSingle "true"
   */
  @AqlPath(
          path = "/content[openEHR-EHR-OBSERVATION.kreftmelding_kolorektalkreft_kjemoterapi_dips.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.kreftmelding_kolorektal_lokalisasjon_av_primaertumor_dips.v1]/items[at0021]/value",
          isSingle = true
  )
  private DvCodedText kliniskSikkerKreft;

  /**
   * Name: "Hensikten med behandlingsplanen"
   * Description: "*"
   * IsSingle "true"
   */
  @AqlPath(
          path = "/content[openEHR-EHR-OBSERVATION.kreftmelding_kolorektalkreft_kjemoterapi_dips.v1]/data[at0001]/events[at0002]/data[at0003]/items[at0354]/items[at0355]/value",
          isSingle = true
  )
  private DvCodedText hensiktenMedBehandlingsplanen;

  /**
   * Name: "CEA"
   * Description: "CEA score. "
   * IsSingle "true"
   */
  @AqlPath(
          path = "/content[openEHR-EHR-OBSERVATION.kreftmelding_kolorektalkreft_kjemoterapi_dips.v1]/data[at0001]/events[at0002]/data[at0003]/items[at0354]/items[openEHR-EHR-CLUSTER.kreftmelding_kolorektal_cea_dips.v1]/items[at0001]/value",
          isSingle = true
  )
  private DvQuantity cea;

  /**
   * Name: "Ikke tatt"
   * Description: "Krysses av dersom CEA ikke er tatt. "
   * IsSingle "true"
   */
  @AqlPath(
          path = "/content[openEHR-EHR-OBSERVATION.kreftmelding_kolorektalkreft_kjemoterapi_dips.v1]/data[at0001]/events[at0002]/data[at0003]/items[at0354]/items[openEHR-EHR-CLUSTER.kreftmelding_kolorektal_cea_dips.v1]/items[at0002]/value",
          isSingle = true
  )
  private DvBoolean ikkeTatt;

  /**
   * Name: "ECOG funksjonsstatus"
   * Description: "Angir ecog funksjonsstatus"
   * IsSingle "true"
   */
  @AqlPath(
          path = "/content[openEHR-EHR-OBSERVATION.kreftmelding_kolorektalkreft_kjemoterapi_dips.v1]/data[at0001]/events[at0002]/data[at0003]/items[at0354]/items[openEHR-EHR-CLUSTER.kreftmelding_kolorektal_ecog_funksjonsstatus_dips.v1]/items[at0001]/value",
          isSingle = true
  )
  private DvCodedText ecogFunksjonsstatus;

  /**
   * Name: "Startdato for behandlingen(dd.mm.åååå)"
   * Description: "*"
   * IsSingle "true"
   */
  @AqlPath(
          path = "/content[openEHR-EHR-OBSERVATION.kreftmelding_kolorektalkreft_kjemoterapi_dips.v1]/data[at0001]/events[at0002]/data[at0003]/items[at0354]/items[at0369]/value",
          isSingle = true
  )
  private DvDateTime startdatoForBehandlingenddMmAaaaaaaa;

  /**
   * Name: "Linje"
   * Description: "*"
   * IsSingle "true"
   */
  @AqlPath(
          path = "/content[openEHR-EHR-OBSERVATION.kreftmelding_kolorektalkreft_kjemoterapi_dips.v1]/data[at0001]/events[at0002]/data[at0003]/items[at0354]/items[at0370]/value",
          isSingle = true
  )
  private DvCodedText linje;

  /**
   * Name: "Årsaker"
   * Description: "*"
   * IsSingle "false"
   */
  @AqlPath(
          path = "/content[openEHR-EHR-OBSERVATION.kreftmelding_kolorektalkreft_kjemoterapi_dips.v1]/data[at0001]/events[at0002]/data[at0003]/items[at0354]/items[at0381]/items[at0417]/value",
          isSingle = false
  )
  private List<DvCodedText> aarsaker;

  /**
   * Name: "Spesifiser årsak"
   * Description: "*"
   * IsSingle "true"
   */
  @AqlPath(
          path = "/content[openEHR-EHR-OBSERVATION.kreftmelding_kolorektalkreft_kjemoterapi_dips.v1]/data[at0001]/events[at0002]/data[at0003]/items[at0354]/items[at0381]/items[at0379]/value",
          isSingle = true
  )
  private DvText spesifiserAarsak;

  /**
   * Name: "Medikamenter"
   * Description: "*"
   * IsSingle "false"
   */
  @AqlPath(
          path = "/content[openEHR-EHR-OBSERVATION.kreftmelding_kolorektalkreft_kjemoterapi_dips.v1]/data[at0001]/events[at0002]/data[at0003]/items[at0354]/items[at0380]/items[at0382]/value",
          isSingle = false
  )
  private List<DvCodedText> medikamenter;

  /**
   * Name: "Spesifiser medikament"
   * Description: "*"
   * IsSingle "true"
   */
  @AqlPath(
          path = "/content[openEHR-EHR-OBSERVATION.kreftmelding_kolorektalkreft_kjemoterapi_dips.v1]/data[at0001]/events[at0002]/data[at0003]/items[at0354]/items[at0380]/items[at0399]/value",
          isSingle = true
  )
  private DvText spesifiserMedikament;

  /**
   * Name: "Meldedato"
   * Description: "Angir medledato for meldingen. "
   * IsSingle "true"
   */
  @AqlPath(
          path = "/content[openEHR-EHR-OBSERVATION.kreftmelding_kolorektalkreft_kjemoterapi_dips.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.kreftmelding_kolorektal_melder_dips.v1]/items[at0001]/value",
          isSingle = true
  )
  private DvDate meldedato;

  /**
   * Name: "Melders navn"
   * Description: "Angir medlers navn. "
   * IsSingle "true"
   */
  @AqlPath(
          path = "/content[openEHR-EHR-OBSERVATION.kreftmelding_kolorektalkreft_kjemoterapi_dips.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.kreftmelding_kolorektal_melder_dips.v1]/items[at0002]/value",
          isSingle = true
  )
  private DvText meldersNavn;

  /**
   * Name: "Melder ID"
   * Description: "Angir melder ID. "
   * IsSingle "true"
   */
  @AqlPath(
          path = "/content[openEHR-EHR-OBSERVATION.kreftmelding_kolorektalkreft_kjemoterapi_dips.v1]/data[at0001]/events[at0002]/data[at0003]/items[openEHR-EHR-CLUSTER.kreftmelding_kolorektal_melder_dips.v1]/items[at0003]/value",
          isSingle = true
  )
  private DvText melderId;

  @Override
  public String getTemplateId() {
    return "Kreftregisteret Kolorektalskjema kjemoterapiV2";
  }
}
